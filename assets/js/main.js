function moreProgram(type){

	var parts = document.querySelectorAll(type);

	for(i=0; i<parts.length; i++){

		console.log(parts[i]);

		var arrow = parts[i].querySelector(".arrow");

		if(arrow){

			arrow.addEventListener("click", function(){

				var more = this.parentElement.parentElement.parentElement.querySelector(".bottom");

				if(more.classList.contains("closed")){

					more.classList.remove("closed");
					more.classList.add("open");
					this.innerHTML="–";

				}else{

					more.classList.add("closed");
					more.classList.remove("open");
					this.innerHTML="+";
				}

			});

		}

	}
}

function program(){

	moreProgram(".event");
	moreProgram(".show");
	moreProgram(".person");
}

function isNowScrolled(posTop, scrollY, bottom){

	return (posTop < scrollY && scrollY < bottom) ? true : false;

}

function onTravauxScroll(scrollY){

	var elements = document.querySelectorAll("#projects .project");


	for(i=0; i<elements.length; i++){

		var posTop = elements[i].offsetTop;
		var height = elements[i].clientHeight;
		var head = elements[i].querySelector(".head");
		var bottom = posTop + height - head.clientHeight;



		if(isNowScrolled(posTop, scrollY, bottom)){

			elements[i].classList.add("fixed");
			elements[i].style.paddingTop=head.clientHeight+"px";

		}else{

			elements[i].classList.remove("fixed");
			elements[i].style.paddingTop=0;

		}

	}
}

function scrollHandling(){

	window.addEventListener("scroll", function(){

		var scrollY = window.pageYOffset;
		
		onTravauxScroll(scrollY);

	});
}

program();
//scrollHandling();
