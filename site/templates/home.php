<?php snippet('header') ?>
<header id='about'>
  <div class='title'>
    <h1><?= $site->title()->html() ?></h1>
    <h2><?= $pages->get('about')->subtitle()->html() ?></h2>
    <h3>Édition <?= yyyy($pages->get('about')->current_year())  ?>
      <br><?= $pages->get('about')->town() ?> (CH)</h3>
    </div>
    <div class='infos'>
      <ul>
        <li>Résidence<br><?= ddmm($pages->get('about')->begin_date()) ?> - <?= ddmm($pages->get('about')->end_date()) ?></li>
        <li>Journée<br>ouverte<br><?= ddmm($pages->get('about')->public_date()) ?></li>
      </ul>
      <ul>
        <li><a href=>Accès</a></li>
        <li><a href="mailto:collectif@facteur.org">Contact</a></li>
      </ul>
    </div>
    <div class='images'>
      <?php foreach($pages->get('about')->images() as $image): ?>
        <img src="<?= $image->url() ?>">
      <?php endforeach ?> 
    </div>
    <div class='about'> 
      <?= $pages->get('about')->about()->kirbytext() ?>
    </div>
  </header>
  <main>
    <section id='program'>
      <header>
        <span class='num'>☼</span>
        <h2>Journée ouverte</h2>
        <h3><?= $pages->get('program')->open_day() ?> <?= ddMonth($pages->get('program')->open_date()) ?></h3>
      </header>
      <div class='about'>
        <?= $pages->get('program')->about() ?>
      </div>
      <div class="events">
        <h3>Évenements</h3>
        <?php foreach($pages->get('program')->children()->sortBy('time', 'asc') as $event): ?>
          <?php if($event->template() == 'project' && $event->type() == '0' || $event->template() == 'event'): ?>
            <div class='event'>
              <ul class='head'>
                <li><?= $event->time() ?></li>
                <h4>
                  <?php if($event->template() == 'project'): ?>
                    <a href="#<?= $event->uid() ?>">
                    <?php endif ?> 
                    <?= $event->title() ?>
                    <?php if($event->template() == 'project'): ?> 
                    </a>
                  <?php endif ?>  
                </h4>
                <?php if($event->template() == 'project'): ?>
                  <li>
                  <?php if($event->medias()->isNotEmpty()): ?>
                    <?= tagsToStr($event->medias(), ',') ?>
                  <?php endif ?>
                  </li>
                  <li><?= twoNum($event->num()) ?><span class='arrow'>+</span></li>
                </ul>
              </ul>
              <ul class='bottom closed'>
                <ul>
                  <?php foreach($event->authors()->split() as $author): ?>
                    <li>
                      <a href="#<?= $author ?>">
                        <?= toAuthor($author) ?>
                      </a>
                    </li>
                  <?php endforeach ?>  
                </ul>
                <?= $event->duration() ?> <?php if($event->duration()->isNotEmpty()){ ?>min<?php } ?>
              </ul>
            <?php endif ?>
          </div>
        <?php endif ?>
      <?php endforeach ?> 
    </div>
    <div class='exhibition'>
      <h3>Exposition</h3>
      <?php foreach($pages->get('program')->children()->visible() as $show): ?>
        <?php if($show->template() == 'project' && $show->type() == '1'): ?>
          <div class='show'>
            <ul class='head'>
             <li><?= twoNum($show->num()) ?></li>
             <h4>
              <a href="#<?= $show->uid() ?>">
                <?= $show->title() ?>
              </a>
            </h4>
            <li>
              <?php if($show->medias()->isNotEmpty()): ?>
              <?= tagsToStr($show->medias(), ',') ?>
            <?php endif ?>
            </li>
            <li>
              <span class='arrow'>+</span>
            </li>
          </ul>
          <ul class='bottom closed'>
            <ul>
              <?php foreach($show ->authors()->split() as $author): ?>
                <li>
                  <a href="#<?= $author ?>">
                    <?= toAuthor($author) ?>
                  </a>
                </li>
              <?php endforeach ?>  
            </ul>
          </ul>
        </div>
      <?php endif ?>
    <?php endforeach ?> 
  </div> 
</section>
<section id='projects'>
  <header>
    <span class='num'>☞</span>
    <h2>Projets</h2>
  </header>
  <?php foreach($pages->get('program')->children()->visible()->filterBy('template', 'project') as $project): ?>
    <div class='project' id='<?= $project->uid() ?>'>
      <ul class='head'>
        <li><?= twoNum($project->num()) ?></li>
        <h3><?= $project->title() ?></h3>
        <li><?= tagsToStr($project->medias(), '<br>') ?></li>
        <li><?php foreach($project->tags()->split() as $tag){ echo "#".$tag." "; } ?></li>
      </ul>
      <?php snippet('projects/body', ["project" => $project]) ?>
    </div>
  <?php endforeach ?>  
</section>
<section id='people'>
  <header>
    <span class='num'>☺</span>
    <h2>Participants</h2>
  </header>
  <?php foreach($pages->get('people')->children()->sortBy('name', 'asc') as $person): ?>
   <div class='letter'>
    <?= getAlpha($pages->get('people')->children()->sortBy('name', 'asc'), $person->name().'-'.$person->uid()) ?>
  </div>
  <div class='person' id='<?= $person->uid() ?>'>
    <ul class='head'>
      <h3>
        <a href="<?= $person->link() ?>" target="_blank">
          <?= $person->title() ?>
        </a>
      </h3>
      <li>
        <span class="coun">
          <?= getCountryCode($person->country(), $pages->get('people')->countries()) ?>
        </span>
        <span class="arrow">
          +
        </span>
      </li>
    </ul>
    <div class="bottom closed">
      <div class='about'>
        <p><?= firstCap($person->about()) ?></p>
      </div>
      <ul class='projects'>
        <?php foreach(getProjects($person->uid(), $pages->get('program')->children()->visible()) as $project): ?>
          <li>
            <a href="#<?= $project->uid() ?>">
              <?= $project->title() ?>
            </a>
          </li>
        <?php endforeach ?> 
      </ul>
    </div>
  </div>
<?php endforeach ?> 
</section>
</main>
<?php snippet('footer') ?>