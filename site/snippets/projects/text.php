        <div class='text'>
        	<ul>
        		<?php foreach($project->authors()->split() as $author): ?>
        			<li>
        				<a href="#<?= $author ?>">
        					<?= toAuthor($author) ?>
        				</a>
        			</li>
        		<?php endforeach ?>  
        	</ul>
        	<?= $project->about()->kirbytext() ?>
        	<?php if(count($project->materials()->split()) > 0): ?>
        		<h5>Avec&thinsp;:<br><?= tagsToStr($project->materials(), ',') ?></h5>
        	<?php endif ?>
        </div>