<?php 

return function($site, $pages, $page) {

	function firstCap($str){

		$first = strtoupper(substr($str, 0, 1));

		return $first.substr($str, 1);
		
	}

	function poss($date){

		$pos1 = strpos($date, '-')+1;
		$pos2 = strrpos($date, '-')+1;

		return [$pos1, $pos2];

	}

	function dd($date){

		$pos = poss($date);

		return substr($date, $pos[1]);

	}

	function day($date){

		$pos = poss($date);
		$yo = substr($date, $pos[1]);

		if(substr($yo, 0, 1) == "0"){

			return substr($yo, 1);

		}else{

			return $yo;
		}

	}

	function mm($date){

		$pos = poss($date);

		return substr($date, $pos[0], 2);

	}

	function yyyy($date){

		$pos = poss($date);

		return substr($date, 0, 4);
	}

	function ddmm($date){

		return dd($date).'.'.mm($date);
	}

	function ddMonth($date){

		$months = ['','janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
		$mm = $months[substr(mm($date), 1)];

		return day($date).' '.$mm;
	}

	function ddmmyyyy($date){

		return dd($date).'.'.mm($date).'.'.yyyy($date);

	}

	function getLetter($num){

		$letters = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

		return $letters[$num-1];
	}

	function toAuthor($str){

		$names = explode('-', $str);
		$finals = [];

		foreach ($names as $name) {
			
			$finals [] = firstCap($name);
		}

		return implode(' ', $finals);
	}

	function authorsToStr($authors, $glue){

		$array = [];

		foreach($authors->split() as $key=>$author){

			if(count($authors->split())-1 !== $key){

				$array [] = toAuthor($author).$glue; 

			}else{

				$array [] = toAuthor($author); 

			} 
		}

		return implode(' ', $array);
	}

	function tagsToStr($tags, $glue){

		$array = [];

		foreach($tags->split() as $key=>$tag){

			if(count($tags->split())-1 !== $key){

				$array [] = firstCap($tag).$glue; 

			}else{

				$array [] = firstCap($tag); 

			} 
		}

		return implode(' ', $array);


	}

	function getCountryCode($country, $countries){

		$back = [];

		foreach($countries->toStructure() as $eky=>$count){

			if(trim($count->name()) == trim($country)){

				$back [] = $count->code();
			}

		}

		return $back[0];
	}

	function getProjects($person, $projects){

		$array = [];

		foreach($projects as $project){

			if(in_array($person, $project->authors()->split())){

				$array [] = $project;
			}
		}

		return $array;
	}

	function showUrl($url){

		$begins = ["http://www.", "https://www.", "http://", "https://"];
		$back = [];

		foreach($begins as $begin){

			$sample = substr($url, 0, strlen($begin));

			if($sample == $begin){

				if(substr($url, strlen($url)-1, strlen($url)-2) == "/"){

					$back []  = substr($url, strlen($begin), strlen($url)-strlen($begin)-1);

				}else{

					$back []  = substr($url, strlen($begin));
				}
				
				
			}
		}

		if(count($back) > 0){

			return $back[0];

		}
		

	}


	function getAlpha($people, $id){

		$firsts = [];
		$ids = [];
		$back = [];
		$letters = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

		foreach($people as $person){

			$firsts [] = substr($person->name(), 0, 1);
			$ids [] = $person->name().'-'.$person->uid();

		}

		$firsts = array_unique($firsts);

		foreach($firsts as $key=>$first){

			$back [] = $ids[$key];


		}

		if(in_array($id, $back)){

			return strtoupper(substr($id, 0, 1));
		}

	}

	function isOdd($num){

		if($num % 2 == 0){

			return true;

		}else{

			return false;
		}
	}

	function twoNum($num){

		if($num < 10){

			return '0'.$num;
		
		}else{

			return $num;
		}
	}

	return compact('ddmm', 'ddmmyyyy', 'ddMonth', 'fullProgram', 'getLetter', 'authorsToStr', 'firstCap', 'showUrl', 'getAlpha', 'isOdd', 'toAuthor');
	


};


?>